<!DOCTYPE HTML>
<html>
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-48102560-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-48102560-1');
    </script>

	<meta charset="UTF-8">
    <meta name="description" content="HOME | Barinderpal Singh Hanspal | M.S. Computer Science Graduate R.I.T.">
	<meta name="keywords" content="Resume, Job, Computer Science, Graduate, Student, Masters, MS, R.I.T., RIT, Rochester Institute of Technology, Full Time, Career, Software, Developer, Development, Engineer, Engineering, Information Technology, Web Development, Java, Spring">
	<meta name="author" content="Barinderpal Singh Hanspal">
    <!-- <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />   -->
	<title>HOME | Barinderpal Singh Hanspal | M.S. Computer Science Graduate, R.I.T.</title>
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="css/style.css" type="text/css">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" href="css/default/default.css" type="text/css" />
	
   <script src="http://code.jquery.com/jquery-1.11.0.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script src="javascript/webscripts.js" type="text/javascript"></script>
    <link href="css/SpryValidationTextField.css" rel="stylesheet" type="text/css">
	<link href="css/SpryValidationTextarea.css" rel="stylesheet" type="text/css">
   
</head>
<body>
	
   <div id="container">
   
	<div id="header">
 
		<div>
			<div class="logo">
				<a href="index.php">Barinder Hanspal<br>
                <font size="-1" >M.S. Computer Science, R.I.T. </font>
                </a>
			</div> 
          <div id="tabs">
                <ul id="navigation">
                    <li>
                        <a href="#" class="active">HOME</a>
                    </li>
                    <li>
                        <a href="#">RESUME</a>
                    </li>
                    <li>
                        <a href="#">PROJECTS</a>
                    </li>
                    <li>
                        <a href="#">CONTACT</a>
                    </li>
                </ul>
			</div>
		</div>
	</div>

	<div id="contents">
		<div id="tagline" class="clearfix">
		  <div id="tab-content">
          
        	  <!-- HOME SECTION -START -->
          
           	  <div id="tab1">&nbsp;
               	<div class="tab-left-content">
              &nbsp;
                    </div>
                    <div class="tab-right-content">
                     <font size="+3">Hi,</font><br/><br/><br/>
					 
					<p>I am a Recent Graduate Student of the Computer Science Department at Rochester Institute of Technology with strong foundations in Object Oriented Design & Programming, Computer Software Fundamentals and Web Application Development</p>
                    <p>My areas of interests include Software Programming, Web Applications, Database Management Systems, Big Data, Map Reduce, Data Structure & Algorithms. </p>
                    <p>I always try to experiment with out-of-the-box approaches to solve problems and strive to find ways to do things efficiently. My ability to pick up new languages/technologies quickly helps me to work in Dynamic / Agile Development environments and successfully contribute to the code-base quickly.</p>
                    <p>I have 4+ years experience in Design and Development of multiple eCommerce platforms at Staples leveraging various open source technologies/frameworks like Spring MVC, Spring Boot, Netflix OSS, Spring Data, Spring Batch, SOLR Cloud, etc.</p>
                    <p>I have interned with Qualcomm and Vicor Corporation in the past & contributed to design, develop and maintain Web based applications. This is what my Managers had to say about my work:</p>
                    <p><i>"I have given Barinderpal very high grades in this evaluation. He is one of the stronger Co-Ops that we have had on our CIM Team, over many years of doing CIM Co-Ops. His work is professional quality, and he effectively leveraged our Team environment. He quickly learned new technology, and has successfully completed 2 assigned complex projects, both of which are similar in scope to projects which I would normally assign to a CIM Engineer",</i><b> - Manager at Vicor Corporation.</b></p>
                    <p><i>"Barinder has been very helpful in working on our projects and has already contributed code that has gone in our production releases. After explaining concepts and goals for our projects, Barinder has shown he is a quick learner and is able to focus on the problems given. He seems to be genuinely interested in learning how to produce quality code and asks great questions that shows he is engaged and understanding things."</i><b> - Manager at Qualcomm.</b></p>
				
                    </div>
            </div>
                 
                 <!-- HOME SECTION -END -->
                 <!-- RESUME SECTION -START -->
                
            <div id="tab2">
                <font class="tab-headers">RESUME
                	<div id="resume-download-button">
             	    	<a href="downloads/RESUME_BARINDER_HANSPAL.pdf" target="_blank" title="Resume of Barinderpal Singh Hanspal">
             	  	 		<img src="images/view_pdf.png" title="View my Resume in PDF format">
                   		</a>
                    </div>
                </font>
                
        	     	<div class="tab-left-content">
        	     	  <div class="resume-left-content">
                      	<hr/>
                        <div class="tab-left-left">
                        	<div id="talkbubble">Objective</div>
                        </div>
                        <div class="tab-left-right">
                        	Seeking a Full Time Software Developer/Engineer position to apply my knowledge in the field of Computer Science & contribute my ideas towards the success of the organization. 
                        </div>
                        <hr/>
                        <div class="tab-left-left">
                        	<div id="talkbubble">Work Experience</div>
                        </div>
                        <div class="tab-left-right">
                        	<div class="work-experience-details">
                            
                            <div class="company-info"> 
                             	<div class="company-info-text">
                                	<div class="company-name">Edelman Financial Engines
                                    	<div class="company-location">Boston, Massachusetts</div>
                                    </div>
                                </div>
                                <div class="company-info-logo">
                                	<a href="https://www.edelmanfinancialengines.com/" target="_blank" title="Edelman Financial Engines">
                                     <img src="images/efelogo.png" align="right"> </a>
                                </div>
                             </div>

                             <div class="job-title">Senior Full Stack Software Engineer</div>
                            <div class="time-period">October 2018 to Present</div>
								<div class="job-descr">
                                	<ul>
                                        <li>Using Agile Software Development methodologies to enhance and customize existing asset management applications using technologies like Java 8, Spring, Angular, SQLServer and AWS</li>
                                        <li>Participating in design and implementation of 3rd party Record Keeper integrations with Edelman Financial Engine applications using Web Services.</li>
                                    </ul>
                                </div>  

                            <hr/>
							 <div class="company-info"> 
                             	<div class="company-info-text">
                                	<div class="company-name">Staples Inc.
                                    	<div class="company-location">Framingham, Massachusetts</div>
                                    </div>
                                </div>
                                <div class="company-info-logo">
                                	<a href="http://www.staples.com/" target="_blank" title="Staples Inc.">
                                     <img src="images/staples_logo.png" align="right"> </a>
                                </div>
                             </div>

                             <div class="job-title">Senior Software Developer</div>
                            <div class="time-period">May 2016 to October 2018</div>
								<div class="job-descr">
                                	<ul>
                                        <li>Using Agile Software Development methodologies to build the next generation Global eCommerce platform for Staples.com, StaplesBusinessAdvantage.com and other tenants leveraging MicroServices architecture</li>
                                        <li>Participating in conceptualization and implementation of platform features and domain level micro-services using technologies like Java 8, RxJava, Spring Boot, Spring Cloud Config, Netflix OSS (Zuul, Hystrix, Eureka, Ribbon), Spring Data, Couchbase, Swagger, Maven, Docker, Azure, GIT, Mockito, JaCoCo.</li>
                                        <li>Using Spring Batch and Spring data to create ETL processes to load feeds into backend databases</li>
                                        <li>Enhance eCommerce platform for StaplesPreferred Canada B2B (Business to Business) web store</li>
                                    </ul>
                                </div>  

                            <div class="job-title">Software Developer</div>
                            <div class="time-period">March 2015 to May 2016</div>
								<div class="job-descr">
                                	<ul>
                                    	<li>Working in an Agile Software Development Environment to build the next generation E-Commerce platform for Staples B2B(Business to Business) Web Stores</li>
                                    	<li>Participating in conceptualization and implementation of  platform features</li>
										<li>Customizing Broadleaf Commerce's open source E-commerce framework (Java/Spring/Hibernate/Thymeleaf/SOLR) for B2B requirements</li>
										<li>Designing and Developing SOLR Search Engine Solution for new framework using SOLR Cloud and Zookeeper</li>
                                    </ul>
                                </div>  
                            <div class="job-title">Software Developer (Contract)</div>
                            <div class="time-period">November 2014 to March 2015</div>
								<div class="job-descr">
                                	<ul>
                                    	<li>Working in an Agile Software Development Environment to build the next generation E-Commerce platform for Staples B2B(Business to Business) Web Stores</li>
                                    	<li>Participating in conceptualization and implementation of  platform features</li>
										<li>Customizing Broadleaf Commerce's open source E-commerce framework (Java/Spring/Hibernate/Thymeleaf/SOLR) for B2B requirements</li>
										<li>Designing and Developing SOLR Search Engine Solution for new framework using SOLR Cloud and Zookeeper</li>
                                    </ul>
                                </div>   
                            
                                <hr/>
                                
                                
							 <div class="company-info"> 
                             	<div class="company-info-text">
                                	<div class="company-name">AppDynamics Inc.
                                    	<div class="company-location">San Francisco, California</div>
                                    </div>
                                </div>
                                <div class="company-info-logo">
                                	<a href="http://www.appdynamics.com/" target="_blank" title="AppDynamics Inc.">
                                     <img src="images/appd_logo.png" align="right"> </a>
                                </div>
                             </div>
                              <div class="job-title">Solutions Engineer</div>
                                <div class="time-period">August 2014 to October 2014</div>
								<div class="job-descr">
                                	<ul>
                                    	<li>Designed & developed innovative solutions for different technology stacks, and used them to 
										integrate AppDynamics APM product into customers application infrastructure</li>
										<li>Provided technical guidance around operations and performance optimizations of application 
										infrastructure of customers (including Fortune 500s)</li>
										<li>Developed and maintained scripts to semi-automate APM tool installation and configuration 
										process, drastically reducing time and manual effort to get it up and running </li>
                                    </ul>
                                </div>   
                                
                                <hr/>
								
                             <div class="company-info"> 
                             	<div class="company-info-text">
                                	<div class="company-name">QUALCOMM Incorporated
                                    	<div class="company-location">San Diego, California</div>
                                    </div>
                                </div>
                                <div class="company-info-logo">
                                	<a href="http://www.qualcomm.com/" target="_blank" title="QUALCOMM Incorporated">
                                     <img src="images/qualcomm_logo.png" align="right"> </a>
                                </div>
                             </div>
                              <div class="job-title">I.T. Application Lifecycle Management Intern</div>
                                <div class="time-period">May 2013 to August 2013</div>
								<div class="job-descr">
                                	<ul>
                                    	<li>Actively participated in SCRUM Agile Software (Full Stack) Development process 
										to develop and maintain Web Application created using PHP, JAVA & Ext JS 4.1</li>
                                        <li>Improved application by using technologies like PDO, LDAP, LOG4PHP, JQuery and AJAX</li>
                                        <li>Improved performance of various modules of the application by as much as 400% by optimizing code, Database  Schemas & SQL statements</li>
                                        <li>Helped identify and correct bugs in existing application.</li>
                                    </ul>
                                </div>   
                                
                                <hr/>
                                <div class="company-info"> 
                                    <div class="company-info-text">
                                        <div class="company-name">VICOR Corporation
                                          <div class="company-location">Andover, Massachusetts</div>
                                        </div>
                                    </div>
                                    <div class="company-info-logo">
                                    	<a href="http://www.vicorpower.com/" target="_blank" title="VICOR Corporation">
                                         <img src="images/vicor_logo.png" align="right"> </a>
                                    </div>
                                </div>
                             	<div class="job-title">CIM Software Developer CO-OP</div>
                                <div class="time-period">November 2012 to May 2013</div>
								<div class="job-descr">
                                	<ul> 
                                    	<li>Worked as a Full Stack Software Engineer to design & develop Web Applications using Spring Web MVC
										(JPA/Hibernate) in a highly entrepreneur environment</li>
                                        <li>Successfully created Spring Web MVC based Web Application from bottom-up to replace slow, manual & legacy data extraction processes; drastically improving end user experience</li>
										<li>Improved existing applications using JavaScript, LOG4J, JQuery, AJAX, ANT & Spring Web MVC </li>
										<li>Contributed to find and correct critical bugs in existing applications</li>
                                        <li>Optimized code and SQL queries to improve responsiveness of existing applications</li>
                                    </ul>
                                </div>   
                                
                                <hr/>
                                
                               	<div class="company-info"> 
                                	<div class="company-info-text">
                                    	<div class="company-name">B. T. Golisano College of Computing and Information Science, R.I.T.
                                        	<div class="company-location">Rochester, New York</div>
                                        </div>
                                   	</div>
                                    <div class="company-info-logo">
                                          <!--<a href="http://www.rit.edu/" target="_blank" title="Rochester Institute of Technology"> 
                                          <img src="images/rit_logo2.png" align="right"> </a>-->
                                    	<a href="http://www.cs.rit.edu/" target="_blank" title="Department of Computer Science at R.I.T.">
                                         <img src="images/cs_rit_logo.png" align="right"></a>
                                  	</div>
                              	</div>
                                <div class="job-title">Lab Assistant</div>
                                <div class="time-period"> August 2013 to May 2014 & <br/> August 2012 to November 2012</div>
                                <div class="job-descr">
                                	<ul>
                                    	<li>Maintaining computer labs and its equipment</li>
                                        <li>Troubleshooting software and hardware issues</li>
                                    </ul>
                                </div>   
                                
                                                        
                          </div>
                        </div>
                        
                         
                         <hr/>
                        <div class="tab-left-left">
                        	<div id="talkbubble">Education</div>
                        </div>
                        <div class="tab-left-right">
                        	<div class="work-experience-details">
                            
                             <div class="company-info"> 
                             	<div class="company-info-text">
                                	<div class="company-name">Rochester Institute of Technology
                                    	<div class="company-location">Rochester, New York</div>
                                    </div>
                                </div>
                                <div class="company-info-logo">
                                	<a href="http://www.rit.edu/" target="_blank" title="Rochester Institute of Technology">
                                     <img src="images/rit_logo2.png" align="right"> </a>
                                </div>
                             </div>
                              <div class="job-title">Masters of Science in Computer Science</div>
                                <div class="time-period">May 2014</div>
								<div class="job-descr">
									<!--<b>Current GPA: 3.44/4.00</b><br/><br/>-->
                                	<p>The program consists of a core curriculum, that helps me improve my knowledge on the
                                     theoretical principles underlying computer science and a variety of clusters & electives
                                      to obtain in-depth knowledge in a computer science discipline. My areas of focus have been Data Management, Data Mining and Distributed Computing.
                                	<br/><br/>
                                    <b><u>Courses</u>:</b>
                                    Computer Science IV (C++), Advanced Programming (Java), Mobile Computing, Foundations of 
                                    Computer Theory, Data Communication and Networks, Intelligent Security Systems, Data 
                                    Exploration and Management, Data Cleaning and Preparation, Database System Implementation,
                                     Data Mining, Foundations of Algorithms, Parallel Computing, Web Services.
									<br/><br/>
                                </div>   
                                
                                <hr/>
                                <div class="company-info"> 
                                    <div class="company-info-text">
                                        <div class="company-name">University of Mumbai
                                          <div class="company-location">Mumbai, India</div>
                                        </div>
                                    </div>
                                    <div class="company-info-logo">
                                    	<a href="http://www.mu.ac.in/" target="_blank" title="University of Mumbai">
                                         <img src="images/mu-logo.png" align="right"> </a>
                                    </div>
                                </div>
                             	<div class="job-title">Bachelor of Engineering in Information Technology</div>
                                <div class="time-period">August 2010</div>
								<div class="job-descr">
                                	<p>The program introduced me to concepts related to software designing & development, 
                                    management of computer based information systems, particularly software applications.
                                    <br/><br/>
                                    <b><u>Relevant Courses</u>:</b>
									Data Structures and Algorithms, Image processing, System Software and Operating Systems,
                                     Computer Organization and Architecture, Computer Simulation and Modeling, Advance 
                                     Database Systems, Management Information System, Project Management, Computer Networks,
                                      Digital Communication
                                </div>   
                                
                               
                                                      
                          </div>
                        </div>
                      </div>
        	     	</div>
                    <div class="tab-right-content">
                    
                    	<div class="right-content-skills-details">
	                        <div id="talkbubble"> Languages </div>
                            <div class="skill-details-text">
                            	Java <br/>
                                C/C++ <br/>
                                JavaScript<br/>
                                PHP <br/>
                                Python <br/>
                                Shell Scripting<br/>
                            </div>
    					</div>
                        
                        <hr/>
                        
                        <div class="right-content-skills-details">
	                        <div id="talkbubble"> Technologies </div>
                            <div class="skill-details-text">
                                Angular/TypeScript<br/>
                                Thymeleaf<br/>
                            	Spring Web MVC <br/>
                                Hibernate/JPA<br/>
                                Spring Boot<br/>
                                Spring Cloud<br/>
                                Spring Batch<br/>
                                Spring Data<br/>
                            	RxJava<br/>
                            	SOLR <br/>
                            	Zookeeper<br/>
				                HTML/HTML5 <br/>
                                CSS/CSS3 <br/>
                                JQuery <br/>
                                JSON <br/>
                                JSP <br/>
                                JSTL <br/>
                                XML <br/>
                                GIT <br/>
                                SVN <br/>
                                ANT Script <br/>
                                AJAX <br/>
                                SOAP<br/>
                                RESTful<br/>
                                Maven <br/>
                                JaCoCo<br/>
                                Mockito<br/>
                                JUnit<br/>
                                Swagger<br/>
                                Kafka<br/>
                            </div>
    					</div>
                        
                        <hr/>
                        
                        <div class="right-content-skills-details">
	                        <div id="talkbubble">Cloud</div>
                            <div class="skill-details-text">
                            	Azure <br/>
								AWS <br/>
                            </div>
    					</div>

                        <hr/>
                        <div class="right-content-skills-details">
	                        <div id="talkbubble"> <font size="-1" >Mobile&nbsp;Programming </font></div>
                            <div class="skill-details-text">
                            	Android <br/>
								iOS <br/>
                            </div>
    					</div>

						<hr/>
                        
                        <div class="right-content-skills-details">
	                        <div id="talkbubble">Database</div>
                            <div class="skill-details-text">
                            	MySQL <br/>
                            	Oracle<br/>
                                SQLServer<br/>
                                Couchbase<br/>
                                Hadoop<br/>
                            </div>
    					</div>
                        
                        <hr/>
                        
                        <div class="right-content-skills-details">
	                        <div id="talkbubble">Tools</div>
                            <div class="skill-details-text">
                            	Eclipse<br/>
                            	IntelliJ IDEA<br/>                         
                            	<font class="reduce-size-font">Spring Tools Suite <br/> </font>
								Hive<br/>
				JRebel<br/>
                                GCC (C++) <br/>
                                Xcode <br/>
                                <font class="reduce-size-font">MS Visual Studio <br/> </font>
                                XAMPP <br/>
                                <font class="reduce-size-font">Adobe Dreameaver<br/> </font>
                            	<font class="reduce-size-font">Toad for Oracle <br/> </font>
                                Weka <br/>
								<font class="reduce-size-font">MS SQL Server <br/> </font>
	                            <font class="reduce-size-font">VisualVM<br/> </font>
                                AWS EC2 <br/>
                                AppDynamics APM <br/>
                            </div>
    					</div>
                        
                        <hr/>
                        
                        <div class="right-content-skills-details">
	                        <div id="talkbubble">Operating Systems</div>
                            <div class="skill-details-text">
                                MS Windows <br/>
                                Unix/Linux <br/>
                                Mac OS <br/>
                            </div>
    					</div>
                        
                    </div>
            	 </div>
                 
                 <!-- RESUME SECTION -END -->
                 <!-- PROJECT SECTION -START -->
                 
	             <div id="tab3">
                      <font class="tab-headers">PROJECTS</font>
                      
                      <div class="project-details">
                    	 <hr/>
                           <div class="tab-left-content">
                           
                                 <div id="talkbubble">Project | 01 </div>
                            </div>
                            <div class="tab-right-content">
                                 <div class="project-title">
                                 	GENERIC PUZZLE SOLVER
                                 </div>
                                 <div class="project-type"> 
                                    	C++ Application
                                 </div>
                                 <div class="project-descr">
                                 	<ul> 
                                    	<li> Programmed a Generic Puzzle Solver using OOP concepts and Breadth First Search algorithm </li>
                                        <li> Memory management, performance enhancement & testing carried out in phases to improve functionality.</li>
                                        <li> Revision Control System (RCS) used for version control. </li>
                                    </ul> 
                                 	<div class="project-languages-used"><u>Languages/Technologies Used</u>:<br/>
                                    	<div id="skill-rectangles">C++</div>
                                    </div>
                                    
                                    <br/>
                                    <div class="project-tools-used"><u>Tools Used</u>:<br/>
                                    	<div id="skill-rectangles">Revision Control System</div>
                                        <div id="skill-rectangles">Gedit</div>
                                    </div>
                                 </div>
                               
                               	 <button class="project-content-toggling-buttons hide" id="project1">See More</button>
                                 
                                 <div id = "more-about-project1" class="more-about-project-class">
                                 	<div class="more-about-project">
                                    
                                	 </div>
                                	 <div class="project-images">
                                    	<!-- <div class="slider-wrapper theme-default"> 
                                       	 	<div class="ribbon"></div> 
                                       			<div id="slider" class="nivoSlider">
                                        
                                               <img src="images/box.png" alt="" /> 
                          
                                               <img src="images/slide2.jpg" alt="" title="#htmlcaption" /> 
                                          <img src="images/bg-button.png" alt="" title="This is an example of a caption" />
                                      
                                        		</div>
                                     		</div> -->
                                    	</div>
                            		</div>
                               </div> <!-- end of more about project 1 -->
                       
                          <!-- Start new project -->
                        <hr/>
                           <div class="tab-left-content">
                           
                                 <div id="talkbubble">Project | 02 </div>
                            </div>
                            <div class="tab-right-content">
                                 <div class="project-title">PARALLEL PAGERANK COMPUTATION </div>
                                  <div class="project-type">Parallel JAVA Application</div>
                                 <div class="project-descr">
                                 		<ul>
                                        	<li>Created a multi-node multi-core Parallel and a single-node single-core
                                             Sequential Java program to calculate the page ranks of web pages in a web
                                              network by performing Monte Carlo simulations.</li>
											<li>Achieved near ideal Strong Scaling and Weak Scaling efficiency for the
                                             Parallel Program</li>
										</ul>				
                                 	<div class="project-languages-used"><u>Languages/Technologies Used</u>:<br/>
                                    	<div id="skill-rectangles">JAVA</div>
                                        <div id="skill-rectangles">
                                        <a href="http://www.cs.rit.edu/~ark/pj2.shtml" target="_blank" name="Parallel Java 2">
                                        Parallel JAVA 2 (PJ2)</a></div>
                                    </div>
                                    
                                    <br/>
                                    <div class="project-tools-used"><u>Tools Used</u>:<br/>
                                    	<div id="skill-rectangles">MyEclipse</div>
                                        <div id="skill-rectangles">Git(Bitbucket)</div>
                                    </div>
                                 </div>
                               
                               	 <button class="project-content-toggling-buttons" id="project2">See More</button>
                                 
                                 <div id = "more-about-project2" class="more-about-project-class">
                                 	<div class="more-about-project">
                                    	More information on this project can be found on the <a href="https://sites.google.com/a/g.rit.edu/csci654-team_orion/" target="_new" name="Project Website for Parallel Computing Project">Project Website</a>
                               	   </div>
                                	 <div class="project-images">
                                    	<!-- <div class="slider-wrapper theme-default"> 
                                       	 	<div class="ribbon"></div> 
                                       			<div id="slider" class="nivoSlider">
                                        
                                               <img src="images/box.png" alt="" /> 
                          
                                               <img src="images/slide2.jpg" alt="" title="#htmlcaption" /> 
                                          <img src="images/bg-button.png" alt="" title="This is an example of a caption" />
                                      
                                        		</div>
                                     		</div> -->
                                    	</div>
                            		</div>
                               </div> 
                               
                               <!-- end of more about project -->
                                              <!-- Start new project -->
                        <hr/>
                           <div class="tab-left-content">
                           
                                 <div id="talkbubble">Project | 03 </div>
                            </div>
                            <div class="tab-right-content">
                                 <div class="project-title">RECOMMENDATION SYSTEM FOR WEB SERVICES</div>
                                  <div class="project-type">Recommendation Application/Algorithm</div>
                                 <div class="project-descr">
                                 		<ul>
                                        	<li>Creating a Web Service Recommendation System that learns the user's Web Service usage patterns and suggests APIs that matches their interests</li>
											<li>Algorithm inspired by the User-Based Collaborative Filtering approach.</li>
											<li>Recommendation performance to be evaluated on the ProgrammableWeb's Web Service(APIs/Mashups) database extracted to a local SQL database with the help of a JAVA RESTful Client</li>
										</ul>				
                                 	<div class="project-languages-used"><u>Languages/Technologies Used</u>:<br/>
                                    	<div id="skill-rectangles">JAVA</div>
                                        <div id="skill-rectangles">RESTful</div>
                                        <div id="skill-rectangles">SQL</div>
                                    </div>
                                    
                                    <br/>
                                    <div class="project-tools-used"><u>Tools Used</u>:<br/>
                                    	<div id="skill-rectangles">MyEclipse</div>
                                        <div id="skill-rectangles">Git(BitBucket)</div>
                                    </div>
                                 </div>
                               
                               	 <button class="project-content-toggling-buttons hide" id="project3">See More</button>
                                 
                                 <div id = "more-about-project3" class="more-about-project-class">
                                 	<div class="more-about-project">
                                	 </div>
                                	 <div class="project-images">
                                    	<!-- <div class="slider-wrapper theme-default"> 
                                       	 	<div class="ribbon"></div> 
                                       			<div id="slider" class="nivoSlider">
                                        
                                               <img src="images/box.png" alt="" /> 
                          
                                               <img src="images/slide2.jpg" alt="" title="#htmlcaption" /> 
                                          <img src="images/bg-button.png" alt="" title="This is an example of a caption" />
                                      
                                        		</div>
                                     		</div> -->
                                    	</div>
                            		</div>
                               </div> 
                               
                               <!-- end of more about project -->        
                       
                        <!-- Start new project -->
                        <hr/>
                           <div class="tab-left-content">
                           
                                 <div id="talkbubble">Project | 04 </div>
                            </div>
                            <div class="tab-right-content">
                                 <div class="project-title">ATM Machine Simulation</div>
                                  <div class="project-type">JAVA Application</div>
                                 <div class="project-descr">
                                 		<ul>
                                        	<li>Simulated an ATM Machine using Swings & Events, Multi-threading and Networking concepts </li>
											<li>Different versions of this program used different networking technologies like TCP/IP, UDP and RMI.</li>
										</ul>				
                                 	<div class="project-languages-used"><u>Languages/Technologies Used</u>:<br/>
                                    	<div id="skill-rectangles">JAVA</div>
                                        <div id="skill-rectangles">TCP/IP</div>
                                        <div id="skill-rectangles">UDP</div>
                                        <div id="skill-rectangles">RMI</div>
                                        <div id="skill-rectangles">Swings/Events</div>
                                        <div id="skill-rectangles">Multi-threading</div>
                                    </div>
                                    
                                    <br/>
                                    <div class="project-tools-used"><u>Tools Used</u>:<br/>
                                    	<div id="skill-rectangles">MyEclipse</div>
                                    </div>
                                 </div>
                               
                               	 <button class="project-content-toggling-buttons hide" id="project4">See More</button>
                                 
                                 <div id = "more-about-project4" class="more-about-project-class">
                                 	<div class="more-about-project">
                                	 </div>
                                	 <div class="project-images">
                                    	<!-- <div class="slider-wrapper theme-default"> 
                                       	 	<div class="ribbon"></div> 
                                       			<div id="slider" class="nivoSlider">
                                        
                                               <img src="images/box.png" alt="" /> 
                          
                                               <img src="images/slide2.jpg" alt="" title="#htmlcaption" /> 
                                          <img src="images/bg-button.png" alt="" title="This is an example of a caption" />
                                      
                                        		</div>
                                     		</div> -->
                                    	</div>
                            		</div>
                               </div> 
                               
                               <!-- end of more about project -->
                       
                       <!-- Start new project -->
                        <hr/>
                           <div class="tab-left-content">
                           
                                 <div id="talkbubble">Project | 05 </div>
                            </div>
                            <div class="tab-right-content">
                                 <div class="project-title">FULL OUTER JOIN IN H2 DATABASE ENGINE & LOGICAL CRACKING OF DATABASE</div>
                                  <div class="project-type">JAVA-WEB & JAVA Application</div>
                                 <div class="project-descr">
                                 		<ul>
                                        	<li>Added full outer join operation capability in H2 database engine</li>
											<li>Simulated adaptive indexing technique in JAVA by logically cracking the databases
.</li>
										</ul>				
                                 	<div class="project-languages-used"><u>Languages/Technologies Used</u>:<br/>
                                    	<div id="skill-rectangles">JAVA</div>
                                        <div id="skill-rectangles">PL/SQL</div>
                                    </div>
                                    
                                    <br/>
                                    <div class="project-tools-used"><u>Tools Used</u>:<br/>
                                    	<div id="skill-rectangles">MyEclipse</div>
                                        <div id="skill-rectangles">Apache Tomcat</div>
                                        <div id="skill-rectangles">H2</div>
                                    </div>
                                 </div>
                               
                               	 <button class="project-content-toggling-buttons hide" id="project5">See More</button>
                                 
                                 <div id = "more-about-project5" class="more-about-project-class">
                                 	<div class="more-about-project">
                                	 </div>
                                	 <div class="project-images">
                                    	<!-- <div class="slider-wrapper theme-default"> 
                                       	 	<div class="ribbon"></div> 
                                       			<div id="slider" class="nivoSlider">
                                        
                                               <img src="images/box.png" alt="" /> 
                          
                                               <img src="images/slide2.jpg" alt="" title="#htmlcaption" /> 
                                          <img src="images/bg-button.png" alt="" title="This is an example of a caption" />
                                      
                                        		</div>
                                     		</div> -->
                                    	</div>
                            		</div>
                               </div> 
                               
                               <!-- end of more about project -->
                               
                             
                               
                                     
                                <!-- Start new project -->
                        <hr/>
                           <div class="tab-left-content">
                           
                                 <div id="talkbubble">Project | 06 </div>
                            </div>
                            <div class="tab-right-content">
                                 <div class="project-title">COMPARISON OF PERFORMANCE OF COMMON SEQUENCE DETERMINING ALGORITHMS</div>
                                  <div class="project-type">JAVA Application</div>
                                 <div class="project-descr">
                                 		<ul>
                                        	<li> Implemented Naive Recursive, a Recursive Algorithm with Memoization, Dynamic
                                             Programming LCS Algorithm, Quadratic-time Linear Space Algorithm to find Longest Common Subsequence of Strings</li>
											<li> Performed CPU and Memory profiling for all algorithms and compared their performances </li>
										</ul>				
                                 	<div class="project-languages-used"><u>Languages/Technologies Used</u>:<br/>
                                    	<div id="skill-rectangles">JAVA</div>
                                    </div>
                                    
                                    <br/>
                                    <div class="project-tools-used"><u>Tools Used</u>:<br/>
                                    	<div id="skill-rectangles">MyEclipse</div>
                                        <div id="skill-rectangles">VisualVM Profiling Tool</div>
                                        <div id="skill-rectangles">Git(Bitbucket)</div>
                                    </div>
                                 </div>
                               
                               	 <button class="project-content-toggling-buttons hide" id="project6">See More</button>
                                 
                                 <div id = "more-about-project6" class="more-about-project-class">
                                 	<div class="more-about-project">
                                	 </div>
                                	 <div class="project-images">
                                    	<!-- <div class="slider-wrapper theme-default"> 
                                       	 	<div class="ribbon"></div> 
                                       			<div id="slider" class="nivoSlider">
                                        
                                               <img src="images/box.png" alt="" /> 
                          
                                               <img src="images/slide2.jpg" alt="" title="#htmlcaption" /> 
                                          <img src="images/bg-button.png" alt="" title="This is an example of a caption" />
                                      
                                        		</div>
                                     		</div> -->
                                    	</div>
                            		</div>
                               </div> 
                               
                               <!-- end of more about project -->
                               
                               
                       <!-- Start new project -->
                        <hr/>
                           <div class="tab-left-content">
                           
                                 <div id="talkbubble">Project | 07 </div>
                            </div>
                            <div class="tab-right-content">
                                 <div class="project-title">iBOT</div>
                                  <div class="project-type">iOS App</div>
                                 <div class="project-descr">
                                 		<ul>
                                        	<li> Designed and developed an iOS App to perform basic file operations on files stored on remote machine.</li>
											<li>Authentication performed using libSSH2 library and secure connection maintained using openSSL library</li>
										</ul>				
                                 	<div class="project-languages-used"><u>Languages/Technologies Used</u>:<br/>
                                    	<div id="skill-rectangles">Objective-C</div>
                                        <div id="skill-rectangles">openSSL</div>
                                        <div id="skill-rectangles">libssh2</div>
                                    </div>
                                    
                                    <br/>
                                    <div class="project-tools-used"><u>Tools Used</u>:<br/>
                                    	<div id="skill-rectangles">Xcode</div>
                                        <div id="skill-rectangles">iPhone SDK</div>
                                    </div>
                                 </div>
                               
                               	 <button class="project-content-toggling-buttons hide" id="project7">See More</button>
                                 
                                 <div id = "more-about-project7" class="more-about-project-class">
                                 	<div class="more-about-project">
                                	 </div>
                                	 <div class="project-images">
                                    	<!-- <div class="slider-wrapper theme-default"> 
                                       	 	<div class="ribbon"></div> 
                                       			<div id="slider" class="nivoSlider">
                                        
                                               <img src="images/box.png" alt="" /> 
                          
                                               <img src="images/slide2.jpg" alt="" title="#htmlcaption" /> 
                                          <img src="images/bg-button.png" alt="" title="This is an example of a caption" />
                                      
                                        		</div>
                                     		</div> -->
                                    	</div>
                            		</div>
                               </div> 
                               
                               <!-- end of more about project -->
                        <!-- Start new project -->
                        <hr/>
                           <div class="tab-left-content">
                           
                                 <div id="talkbubble">Project | 08 </div>
                            </div>
                            <div class="tab-right-content">
                                 <div class="project-title">MovieDB</div>
                                  <div class="project-type">Movie Database Web Application</div>
                                 <div class="project-descr">
                                 		<ul>
                                        	<li>Designed and developed a Movie Database Management System using SQL and Web Technologies. </li>
											<li>Users could access and modify Movie information stored in a SQL database using a web interface.</li>
										</ul>				
                                 	<div class="project-languages-used"><u>Languages/Technologies Used</u>:<br/>
                                    	<div id="skill-rectangles">PHP</div>
                                        <div id="skill-rectangles">HTML</div>
                                        <div id="skill-rectangles">CSS</div>
                                        <div id="skill-rectangles">JavaScript</div>
                                        <div id="skill-rectangles">jQuery</div>
                                        <div id="skill-rectangles">PL/SQL</div>
                                        <div id="skill-rectangles">jSON</div>
                                    </div>
                                    
                                    <br/>
                                    <div class="project-tools-used"><u>Tools Used</u>:<br/>
                                    	<div id="skill-rectangles">phpMyAdmin</div>
                                        <div id="skill-rectangles">Adobe Dreamweaver CS5.5</div>
                                        <div id="skill-rectangles">Apache Tomcat Server</div>
                                        <div id="skill-rectangles">XAMPP</div>
                                    </div>
                                 </div>
                               
                               	 <button class="project-content-toggling-buttons hide" id="project8">See More</button>
                                 
                                 <div id = "more-about-project8" class="more-about-project-class">
                                 	<div class="more-about-project">
                                	 </div>
                                	 <div class="project-images">
                                    	<!-- <div class="slider-wrapper theme-default"> 
                                       	 	<div class="ribbon"></div> 
                                       			<div id="slider" class="nivoSlider">
                                        
                                               <img src="images/box.png" alt="" /> 
                          
                                               <img src="images/slide2.jpg" alt="" title="#htmlcaption" /> 
                                          <img src="images/bg-button.png" alt="" title="This is an example of a caption" />
                                      
                                        		</div>
                                     		</div> -->
                                    	</div>
                            		</div>
                               </div> 
                               
                               <!-- end of more about project -->
                                 
                                     
                       
                                <!-- Start new project -->
                        <hr/>
                           <div class="tab-left-content">
                           
                                 <div id="talkbubble">Project | 09 </div>
                            </div>
                            <div class="tab-right-content">
                                 <div class="project-title">COMPARISON OF DATA MINING TOOLS FOR CLASSIFICATION PERFORMANCE</div>
                                  <div class="project-type">Research Report</div>
                                 <div class="project-descr">
                                 		<ul>
                                        	<li>Compared classification performance and accuracy of OneR, NaiveBayes & 
                                            J48 algorithm on data mining tools like WEKA, R, MS SQL Server 2012, SAS 
                                            Enterprise Miner & Rapidminer using census income dataset.</li>
											<li>Benchmark performed using Amazon Web Service(AWS)</li>
										</ul>				
                                 	
                                    <div class="project-tools-used"><u>Tools Used</u>:<br/>
                                    	<div id="skill-rectangles">WEKA</div>
                                        <div id="skill-rectangles">R</div>
                                        <div id="skill-rectangles">MS SQL Server 2012</div>
                                        <div id="skill-rectangles">SAS Enterprise Miner</div>
                                        <div id="skill-rectangles">Rapidminer</div>
                                        <div id="skill-rectangles">Amazon Web Service(AWS)</div>
                                    </div>
                                 </div>
                               
                               	 <button class="project-content-toggling-buttons hide" id="project9">See More</button>
                                 
                                 <div id = "more-about-project9" class="more-about-project-class">
                                 	<div class="more-about-project">
                                	 </div>
                                	 <div class="project-images">
                                    	<!-- <div class="slider-wrapper theme-default"> 
                                       	 	<div class="ribbon"></div> 
                                       			<div id="slider" class="nivoSlider">
                                        
                                               <img src="images/box.png" alt="" /> 
                          
                                               <img src="images/slide2.jpg" alt="" title="#htmlcaption" /> 
                                          <img src="images/bg-button.png" alt="" title="This is an example of a caption" />
                                      
                                        		</div>
                                     		</div> -->
                                    	</div>
                            		</div>
                               </div> 
                               
                               <!-- end of more about project -->
                               
                                     <!-- Start new project -->
                        <hr/>
                           <div class="tab-left-content">
                           
                                 <div id="talkbubble">Project | 10 </div>
                            </div>
                            <div class="tab-right-content">
                                 <div class="project-title">MINING ACCESS PATTERNS FROM WEB LOGS USING CLUSTERING</div>
                                  <div class="project-type">VB Application</div>
                                 <div class="project-descr">
                                 		<ul>
                                        	<li>Proposed a system where k-means clustering is applied to Web logs to
                                             extract Web usage information.</li>
											<li>MySQL was used to manage the database and Visual Basics was used to
                                             display results analysis results </li>
										</ul>				
                                 	<div class="project-languages-used"><u>Languages/Technologies Used</u>:<br/>
                                    	<div id="skill-rectangles">Visual Basic</div>
                                        <div id="skill-rectangles">PL/SQL</div>
                                    </div>
                                    
                                    <br/>
                                    <div class="project-tools-used"><u>Tools Used</u>:<br/>
                                    	<div id="skill-rectangles">Microsoft Visual Studio</div>
                                        <div id="skill-rectangles">MySQL</div>
                                    </div>
                                 </div>
                               
                               	 <button class="project-content-toggling-buttons hide" id="project10">See More</button>
                                 
                                 <div id = "more-about-project10" class="more-about-project-class">
                                 	<div class="more-about-project">
                                	 </div>
                                	 <div class="project-images">
                                    	<!-- <div class="slider-wrapper theme-default"> 
                                       	 	<div class="ribbon"></div> 
                                       			<div id="slider" class="nivoSlider">
                                        
                                               <img src="images/box.png" alt="" /> 
                          
                                               <img src="images/slide2.jpg" alt="" title="#htmlcaption" /> 
                                          <img src="images/bg-button.png" alt="" title="This is an example of a caption" />
                                      
                                        		</div>
                                     		</div> -->
                                    	</div>
                            		</div>
                               </div> 
                               
                               <!-- end of more about project -->
                               
                                     <!-- Start new project -->
                        <hr/>
                           <div class="tab-left-content">
                           
                                 <div id="talkbubble">Project | 11 </div>
                            </div>
                            <div class="tab-right-content">
                                 <div class="project-title">INTRUSION DETECTION SYSTEM</div>
                                  <div class="project-type">Research Report</div>
                                 <div class="project-descr">
                                 		<ul>
                                        	<li>Used data mining techniques to generate rules for an Intrusion Detection System</li>
											<li>Analyzed and compared many classification techniques including J48 &
                                             Multilayer Perceptron for efficiency.</li>
                                            <li>Improved efficiency and correctness of rules in various phases.</li>
										</ul>	
                                    <div class="project-tools-used"><u>Tools Used</u>:<br/>
                                    	<div id="skill-rectangles">WEKA</div>
                                    </div>
                                 </div>
                               
                               	 <button class="project-content-toggling-buttons hide" id="project11">See More</button>
                                 
                                 <div id = "more-about-project11" class="more-about-project-class">
                                 	<div class="more-about-project">
                                	 </div>
                                	 <div class="project-images">
                                    	<!-- <div class="slider-wrapper theme-default"> 
                                       	 	<div class="ribbon"></div> 
                                       			<div id="slider" class="nivoSlider">
                                        
                                               <img src="images/box.png" alt="" /> 
                          
                                               <img src="images/slide2.jpg" alt="" title="#htmlcaption" /> 
                                          <img src="images/bg-button.png" alt="" title="This is an example of a caption" />
                                      
                                        		</div>
                                     		</div> -->
                                    	</div>
                            		</div>
                               </div> 
                               
                               <!-- end of more about project -->
                               
                                            <!-- Start new project -->
                        <hr/>
                           <div class="tab-left-content">
                           
                                 <div id="talkbubble">Project | 12 </div>
                            </div>
                            <div class="tab-right-content">
                                 <div class="project-title">CLEANING AND PRE-PROCESSING FOR WEB MINING</div>
                                  <div class="project-type">Research Report</div>
                                 <div class="project-descr">
                                 		<ul>
                                        	<li> Discusses importance of pre-processing in web mining process. Reviewed 
                                            and compared various pre-processing techniques used for web structure mining,
                                             web content mining and web usage mining.</li>
										</ul>				
                                 	<!--<div class="project-languages-used"><u>Languages/Technologies Used</u>:<br/>
                                    	<div id="skill-rectangles">Visual Basic</div>
                                        <div id="skill-rectangles">PL/SQL</div>
                                    </div>
                                    
                                    <br/>
                                    <div class="project-tools-used"><u>Tools Used</u>:<br/>
                                    	<div id="skill-rectangles">Microsoft Visual Studio</div>
                                        <div id="skill-rectangles">MySQL</div>
                                    </div> -->
                                 </div>
                               
                               	 <button class="project-content-toggling-buttons hide" id="project12">See More</button>
                                 
                                 <div id = "more-about-project12" class="more-about-project-class">
                                 	<div class="more-about-project">
                                	 </div>
                                	 <div class="project-images">
                                    	<!-- <div class="slider-wrapper theme-default"> 
                                       	 	<div class="ribbon"></div> 
                                       			<div id="slider" class="nivoSlider">
                                        
                                               <img src="images/box.png" alt="" /> 
                          
                                               <img src="images/slide2.jpg" alt="" title="#htmlcaption" /> 
                                          <img src="images/bg-button.png" alt="" title="This is an example of a caption" />
                                      
                                        		</div>
                                     		</div> -->
                                    	</div>
                            		</div>
                               </div> 
                               
                               <!-- end of more about project -->
                                                       <!-- Start new project -->
                        <hr/>
                           <div class="tab-left-content">
                           
                                 <div id="talkbubble">Project | 13 </div>
                            </div>
                            <div class="tab-right-content">
                                 <div class="project-title">ANALYSIS OF SECURITY MECHANISMS IN VIRTUAL PRIVATE NETWORKS</div>
                                  <div class="project-type">Research Report</div>
                                 <div class="project-descr">
                                 		<ul>
                                        	<li>Documented a research paper on the threats in a Virtual Private Network 
                                            in a business environment and various security mechanisms that can be implemented.</li>
										</ul>				
                                 	<!--<div class="project-languages-used"><u>Languages/Technologies Used</u>:<br/>
                                    	<div id="skill-rectangles">Visual Basic</div>
                                        <div id="skill-rectangles">PL/SQL</div>
                                    </div>
                                    
                                    <br/>
                                    <div class="project-tools-used"><u>Tools Used</u>:<br/>
                                    	<div id="skill-rectangles">Microsoft Visual Studio</div>
                                        <div id="skill-rectangles">MySQL</div>
                                    </div> -->
                                 </div>
                               
                               	 <button class="project-content-toggling-buttons hide" id="project13">See More</button>
                                 
                                 <div id = "more-about-project13" class="more-about-project-class">
                                 	<div class="more-about-project">
                                	 </div>
                                	 <div class="project-images">
                                    	<!-- <div class="slider-wrapper theme-default"> 
                                       	 	<div class="ribbon"></div> 
                                       			<div id="slider" class="nivoSlider">
                                        
                                               <img src="images/box.png" alt="" /> 
                          
                                               <img src="images/slide2.jpg" alt="" title="#htmlcaption" /> 
                                          <img src="images/bg-button.png" alt="" title="This is an example of a caption" />
                                      
                                        		</div>
                                     		</div> -->
                                    	</div>
                            		</div>
                               </div> 
                               
                               <!-- end of more about project -->
							   
							      
                                          
                       </div>
        	     </div>
                 
                 <!-- PROJECTS SECTION -END -->
                 <!-- CONTACT SECTION -START -->
                 
                  <div id="tab4">
                  <font class="tab-headers">CONTACT</font>
                  
    	          <div class="tab-left-content">
                  
    	            <div id="my-email">
                    <div id="resume-download-button"> MY RESUME &nbsp;
                     <a href="downloads/RESUME_BARINDER_HANSPAL.pdf" target="_blank" title="Resume of Barinderpal Singh Hanspal">
                     <img src="images/view_pdf.png" hspace="0" align="absmiddle" width="100" height="30" title="View my Resume in PDF format"></a></div><br/><br/>
                     
                    <!-- <div id="resume-download-button"> MY C.V. &nbsp;
                     <a href="downloads/CV_BARINDERPAL_HANSPAL.pdf" target="_blank" title="C. V. of Barinderpal Singh Hanspal">
                     <img src="images/view_pdf.png"  hspace="0" align="absmiddle" width="100" height="30" title="View my C.V. in PDF format">
                     </a>
                    </div> -->
                     <br/><br/><br/><br/><br/><br/>
                     <!-- <a href="http://www.linkedin.com/in/barinderpalhanspal/" target="_blank">View LinkedIn Profile</a><br/><br/> -->
                    <img src="images/icon-email.png" alt="Email" width="24" height="24" hspace="0" align="absmiddle">barinder.hanspal@gmail.com</img><br/><br/>
                    <img src="images/icon-email.png" alt="Email" width="24" height="24" hspace="0" align="absmiddle">LinkedIn: </img><a href="https://www.linkedin.com/in/barinderhanspal/" target="_blank" title="My LinkedIn Profile">barinderhanspal</a><br/><br/>
                    
                    </div>
                   
    	          </div>
                    <div class="tab-right-content">
                      <form name="form1" method="post" action="" id="contactform">
                        <div id="greeting">
                        	<br/><b>Interested in my profile? Have any suggestions?<br/>Leave me a message and I'll get back to you.
                            </b></div>
                        <table style="max-width:300px;" width="250" border="0" align="center" cellpadding="8" cellspacing="2" >
                          <tr>
                            <th align="right" valign="baseline" style="padding-top:14px" scope="row">NAME</th>
                            <td valign="middle"><span id="nameTextFieldsSry">
                                    <input name="name" class="textbox" type="text" id="name" size="40" maxlength="100">
                                        <span class="textfieldRequiredMsg">Please enter your name.</span>
                                        <span class="textfieldMinCharsMsg">Minimum number of characters not met. Please enter atleast 2 characters</span>
                                        <span class="textfieldMaxCharsMsg">Exceeded maximum number of characters.</span>
                                </span>
                                </td>
                          </tr>
                          <tr>
                            <th align="right" valign="baseline" style="padding-top:14px" scope="row"><label for="email">EMAIL</label></th>
                            <td><span id="emailSpryTextField">
                            <input name="email" class="textbox" type="text" id="email" size="40" maxlength="100">
                            <span class="textfieldRequiredMsg">Please enter your email address.</span>
                            <span class="textfieldInvalidFormatMsg">Invalid email address format.</span>
                            <span class="textfieldMinCharsMsg">Minimum number of characters not met.</span>
                            <span class="textfieldMaxCharsMsg">Exceeded maximum number of characters.</span></span></td>
                          </tr>
                          <tr>
                            <th align="right" valign="top" style="padding-top:14px" scope="row">MESSAGE</th>
                                <td>   
                                    <span id="messageSpryTextArea">
                                        <textarea name="message" class="textarea" cols="35" rows="5" id="message"></textarea>
                                            <font size="-1" >Characters left: </font> <span id="countmessageSpryTextArea">&nbsp;</span> 
                                                <span class="textareaRequiredMsg"><br/>Please enter your message.</span>
                                                <span class="textareaMaxCharsMsg"><br/>Exceeded maximum number of characters.</span>
                                       </span>
                              </td>
                          </tr>
                          <tr>
                            <th align="right" valign="baseline" style="padding-top:14px" scope="row">
                            <label for="botcheck">What is 10 + 3?</label></th>
                            <td><span id="botcheckSpryTextField">
                           		 <input name="botcheck" class="textbox" type="text" id="botcheck" size="40" maxlength="100">
                            </span></tr>
                          
                          <tr >
                            <th valign="top" scope="row">&nbsp;</th>
                            <td><div align="left">
                              <input type="submit" name="message-submit" id="message-submit" value="Submit" >
                              <input type="reset" value="Reset" id="reset">
                              
                            </div></td>
                          </tr>
                        </table>
                      </form>
                    </div>
        	     </div>
                 
                 <!-- CONTACT SECTION -END -->
            </div>
        </div>
    </div>
	<div id="footer">
		<div class="clearfix">
		  <div id="connect">
			  <a href="http://www.linkedin.com/in/barinderhanspal/" target="_blank" class="linkedin">
              		<img id="linkedin-image" src="images/linkedin5.gif"  hspace="0" align="absmiddle"
                     height="55" onmouseover="changeImage('images/linkedin5effect.gif')" onMouseOut="changeImage('images/linkedin5.gif')"></img>
              </a>
			</div>
			<font color="#CCCCCC">Website Designed, Developed and Copyright - Barinderpal Singh Hanspal. <br/>
			All images and logos are copyright and/or trademark of their respective owners </font>
</div>
	</div>
        </div>
         
    <script src="javascript/SpryValidationTextField.js" type="text/javascript"></script>
    <script src="javascript/SpryValidationTextarea.js" type="text/javascript"></script>
    	<script type="text/javascript">

			initializeNavigationHandler();
			var sprytextfield1 = new Spry.Widget.ValidationTextField("nameTextFieldsSry", "none", {validateOn:["blur"], minChars:2, maxChars:100});
			var sprytextfield2 = new Spry.Widget.ValidationTextField("emailSpryTextField", "email", {maxChars:90, validateOn:["blur"]});
			var sprytextfield3 = new Spry.Widget.ValidationTextField("botcheckSpryTextField", "integer", {minChars:2, maxChars:2, minValue:13, maxValue:13, validateOn:["blur"], useCharacterMasking:true});
			var sprytextarea1 = new Spry.Widget.ValidationTextarea("messageSpryTextArea", {validateOn:["blur"], counterId:"countmessageSpryTextArea", minChars:1, maxChars:500, counterType:"chars_remaining"});
							    
		  function changeImage(img){
			  setTimeout(function() {document.getElementById('linkedin-image').src=img;},100);
		  }
				
        </script>
        </div>
</body>
</html>