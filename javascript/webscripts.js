// JavaScript Document

function initializeNavigationHandler() {
	
	$('#tab-content div').hide();
			$('#tab-content div:first').show();
			$('#tab-content div:first .tab-left-content').show();
			$('#tab-content div:first .tab-right-content').show();
			
			$('#navigation li').click(function() {
	
					$('#navigation li a').removeClass("active");
					$(this).find('a').addClass("active");
					$('#tab-content div').hide();
			
					var indexer = $(this).index() + 1; //gets the current index of (this) which is #nav li
					$('#tab-content #tab' + indexer).fadeIn(600); //uses whatever index the link has to open the corresponding box 
					$('#tab-content #tab' + indexer + ' *').fadeIn(200);
					$('#nameTextFieldsSry span').removeAttr('style');
					$('#emailSpryTextField span').removeAttr('style');
					$('#messageSpryTextArea span').removeAttr('style');
					if(indexer == 3) {
						$(".more-about-project-class").hide();
						$(".project-content-toggling-buttons").text(function(index, text) {
                            return "See More"
                        });
					}
			});
			
			$(document).ready(function(e) {
				$(".project-content-toggling-buttons").click(function(e) {
					$("#more-about-" + this.id).slideToggle(400, "swing");
					$(this).text(function(index, text) {
						 return text === 'See More' ? 'See Less' : 'See More'
					 });
				});
				
				$('#contactform').submit(function(){
 					if(Spry){
						var contactForm = document.getElementById("contactform");
						if(Spry.Widget.Form.validate(contactForm) == true){
							$.post("sendemail.php", $("#contactform").serialize(),  function(data) {
								if(" " + data !== " SpryIsDown") {
									alert(" " + data);
									$("#reset").click();
								}
							});
							return false;
						}
					}
				});
			});
	}
	
	  
   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-55627620-1', 'auto');
  ga('send', 'pageview');

